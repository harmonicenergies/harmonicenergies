<?php
    /*
    Template Name: Template 2
     */
?>


<?php get_header() ?>
<?php while(have_posts()) : the_post() ; ?>
<?php
$thumb_id = get_post_thumbnail_id();
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'page-header', true);
$thumb_url = $thumb_url_array[0];
$color = get_post_meta(get_the_ID(), 'thought_point_color', true);
$blog = wp_get_recent_posts( array( 'numberposts' => 1) );
?>
<!--
We position the images fixed and therefore need to place them outside of #skrollr-body.
We will then use data-anchor-target to display the correct image matching the current section (.gap element).
-->
<div
class="parallax-image-wrapper parallax-image-wrapper-100"
data-anchor-target="#tagline + .gap"
data-bottom-top="transform:translate3d(0px, 200%, 0px)"
data-top-bottom="transform:translate3d(0px, 0%, 0px)"
>
	<div
		class="parallax-image parallax-image-100"
			style="background-image:url(<?php echo $thumb_url; ?>)"
		data-anchor-target="#tagline + .gap"
		data-bottom-top="transform: translate3d(0px, -80%, 0px);"
		data-top-bottom="transform: translate3d(0px, 80%, 0px);"
	></div>
<!--the +/-80% translation can be adjusted to control the speed difference of the image-->
</div>
<div id="skrollr-body" class="internal-page">
	<div class="" id="tagline" data-80-top="opacity: 0;" data-center="opacity: 1">
		<div class="wrap cf">
			<div class="m-all t-all d-all">
				<h1 class="page-header"><?php echo get_post_meta(get_the_ID(), 'tagline', true); ?></h1>
				<div class="scroll-down"><a href="#page-content"><img src="<?php echo get_template_directory_uri(); ?>/library/images/scroll-down.gif"</img></a></div>
			</div>
		</div>
	</div>
	<div class="gap gap-100" style="background-image:url(<?php echo $thumb_url; ?>)"></div>
	<div id="page-content" class="section">
		<div class="wrap cf"><h2 class="section-title"><?php echo the_title(); ?></h2></div>
                <div class="wrap cf">
			<div class="m-all t-all d-1of2 main-content">
				<?php the_content(); ?>
			</div>
			<div class="m-all t-all d-1of2">
			        <div id="thought-point" class="<?php echo $color; ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/<?php echo $color; ?>.png"></img><br />
<?php echo get_post_meta(get_the_ID(), 'bucket_tagline', true); ?>
                                </div>
			</div>
                </div>
	</div>
	<div id="page-buckets" class="wrap cf">
	<div class="bucket-left m-all t-all d-1of3 testimonials"><hr></hr><h5>Seen and Heard</h5>
	<?php switch (get_the_ID()) {
	case 34: //seo/ppc
		echo "\"Harmonic Energies is a leader and pioneer in search engine marketing as well as a top notch service provider. I have worked with them on a number of online marketing initiatives for clients and they have shown exceptional professionalism, knowledge, and an ability to make me feel as though my time was their top priority. I would definitely recommend Harmonic to anyone looking to utilize them in any capacity.\"<br /><strong>Gabe Elliot</strong><br /><strong>Account Executive, Yahoo!</strong>";
		break;
	case 37: //social media
		echo "\"As nebulous and quickly changing as internet marketing and advertising can be, Harmonic Energies displayed a tremendous amount of comprehension in their proactive approach in learning about new products and opportunities in order to better service their clients. I would certainly refer any business that desires to harness the power of the internet to Harmonic’s capable hands.\"<br /><strong>Josh McCaughey</strong><br /><strong>Account Executive, Yahoo!</strong>";
		break;
	case 43: //roi
		echo "\"Harmonic Energies stays on the cutting edge of Google updates and products, and though they usually agreed with our recommendations, they were sure to ask tough questions to make sure those recommendations were right for the client. Harmonic Energies is a great partner and advocate, and gets results for their customers.\" <br /><strong>Charlae Washington</strong><br /><strong> Google, Inc.</strong>";
		break;
	default:
		echo "\"Harmonic Energies’ ability to quickly understand our company’s sales goals, target audiences, and services truly set their company apart from their competitors. Their seamless process of website design, implementation, and marketing campaigns has given our company an end product that has shown direct results in our sales and client’s education.\" <strong><i>Powerhouse Recycling</i></strong>";
	} ?>

</div>
	<div class="bucket-center m-all t-all d-1of3">
            <hr></hr>
	    <h5>LATEST NEWS</h5> 
	    <?php foreach ($blog as $b) { echo get_the_post_thumbnail($b["ID"], 'service-thumb') . '<div id="title-wrap"><a class="page-title" href="'. get_the_permalink($b["ID"]) . '">' . get_the_title($b["ID"]) . '</a></div>'; }?>
        </div>
	<div class="bucket-right m-all t-all d-1of3"><hr></hr><h5>We'd love to meet you</h5>The best part of our job is the relationships we have formed with clients over the years. We would love to meet with you and learn about what you do and why you do it. Whether you’re starting a business for the first time or you're a nationally recognized brand, we are here to help bring you closer to your customers than ever before. <a href="/contact-us/" class="read-more contact-button">Let's Talk</a></div>
        </div>
</div>
<?php endwhile; ?>
<?php get_footer(); ?>
