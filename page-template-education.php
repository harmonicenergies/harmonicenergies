<?php
    /*
    Template Name: Education
     */
?>


<?php get_header() ?>
<?php while(have_posts()) : the_post() ; ?>
<?php
$thumb_id = get_post_thumbnail_id();
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'page-header', true);
$thumb_url = $thumb_url_array[0];
$color = get_post_meta(get_the_ID(), 'thought_point_color', true);
?>
<!--
We position the images fixed and therefore need to place them outside of #skrollr-body.
We will then use data-anchor-target to display the correct image matching the current section (.gap element).
-->
<div
class="parallax-image-wrapper parallax-image-wrapper-100"
data-anchor-target="#tagline + .gap"
data-bottom-top="transform:translate3d(0px, 200%, 0px)"
data-top-bottom="transform:translate3d(0px, 0%, 0px)"
>
	<div
		class="parallax-image parallax-image-100"
			style="background-image:url(<?php echo $thumb_url; ?>)"
		data-anchor-target="#tagline + .gap"
		data-bottom-top="transform: translate3d(0px, -80%, 0px);"
		data-top-bottom="transform: translate3d(0px, 80%, 0px);"
	></div>
<!--the +/-80% translation can be adjusted to control the speed difference of the image-->
</div>
<div id="skrollr-body" class="internal-page education">
	<div class="" id="tagline" data-80-top="opacity: 0;" data-center="opacity: 1">
		<div class="wrap cf">
			<div class="m-all t-all d-all">
				<h1 class="page-header"><?php echo get_post_meta(get_the_ID(), 'tagline', true); ?></h1>
				<div class="scroll-down"><a href="#page-content"><img src="<?php echo get_template_directory_uri(); ?>/library/images/scroll-down.gif"</img></a></div>
			</div>
		</div>
	</div>
	<div class="gap gap-100" style="background-image:url(<?php echo $thumb_url; ?>)"></div>
</div>
<div class="internal-page education">
	<div id="page-content" class="section">
		<div class="wrap cf"><h2 class="section-title"><?php echo the_title(); ?></h2></div>
                <div class="wrap cf">
			<div class="m-all t-all d-all main-content">
				<?php the_content(); ?>
			</div>
                </div>
	</div>
	<div id="page-buckets" class="wrap cf courses">
	<div id="" class="m-none t-none d-1of3 bucket-left"><hr></hr></div>
	<div  class="m-none t-none d-1of3 bucket-center"><hr></hr></div>
	<div class="m-none t-none d-1of3 bucket-right"><hr></hr></div>
	<div class="wrap cf">
	<div class="m-all t-all d-1of3 bucket-left"><h3>Online Marketing 101 (2 hours)</h3>General introduction to online marketing and what it means. Covers a brief introduction to search engine optimization as well as marketing through Google & online tools including Google Analytics, Google Places, Google Insights for Search, Google Alerts and more. (Recommended for every client)</div>
	<div class="m-all t-all d-1of3 bucket-center"><h3>Google Local/Places (1 hour)</h3>Businesses that service a local customer base must be sure to utilize Google Local. This is the map that you see when doing a search for a local term like "Accountant New York" on Google. This product is also where all the Google Maps information comes from so you'll want to make sure all pertinent information is accurate. Something as simple as an incorrect address can lead to thousands of dollars of lost revenue.</div>
	<div class="m-all t-all d-1of3 bucket-right"><h3>AdWords I (2 hours)</h3>This course explains how businesses can look to open a whole new market and touch a whole new customer base through Google and other online networks. Adwords is an all-inclusive marketing behemoth with lots of switches and levers. We'll make sure you know how to get started by providing you with a basic level of understanding about the product.</div>
	</div>
	<div class="wrap cf">
	    <div class="m-all t-all d-1of3 bucket-left"><h3>AdWords II (2 hours)</h3>Google Analytics is a powerful tool and this seminar dives into ways that you can most effectively utilize this tool. With dozens of metrics and hundreds of reports, Analytics can become a bit overwhelming. We will show you the right metrics to focus on for your business and dive right into your account to review your website's performance.</div>
	    <div class="m-all t-all d-1of3 bucket-center"><h3>Analytics I (1 hour)</h3>We cover an introduction to Google Analytics and how to integrate it into your website. We show you the basics of utilizing Analytics to track website performance and visitor metrics such as visits, page views, bounce rate, conversion rate and more.</div>
	    <div class="m-all t-all d-1of3 bucket-right"><h3>Analytics II (2 hours)</h3>Google Analytics is a powerful tool and this seminar dives into ways that you can most effectively utilize this tool. With dozens of metrics and hundreds of reports, Analytics can become a bit overwhelming. We will show you the right metrics to focus on for your business and dive right into your account to review your websites performance.</div>
        </div>


        </div>
</div>
<?php endwhile; ?>
<?php get_footer(); ?>
