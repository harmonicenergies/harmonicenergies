<?php
    /*
    Template Name: Relationships
     */
?>


<?php get_header() ?>
<?php while(have_posts()) : the_post() ; ?>
<div class="relationships">
<div class="featured-image"><?php the_post_thumbnail('full'); ?>
    <div class="page-header-wrap">
				<h1 class="page-header"><?php echo get_post_meta(get_the_ID(), 'tagline', true); ?></h1>
    </div>
</div>
<div class="internal-page">
		<div class="wrap cf">
		<div class="wrap cf"><h2 class="section-title"><?php echo the_title(); ?></h2></div>
		</div>
	</div>
	<div id="page-content" class="section">

<?php $projects=new WP_Query(array('post_type' => 'project', 'posts_per_page' => 20)); 


$cols = 2;
$c = 1;


        if( $projects->have_posts() ) {
	        while ($projects->have_posts()) : $projects->the_post();

                if ($cols==2) { $rowClass="odd"; } else { $rowClass="even";};
           
		if ($c == 1) {
			echo "<div class='row-$rowClass'>";
			$colClass="first";
		}
		
		if ($c == $cols) {$colClass="last";};
                $link = get_post_meta(get_the_ID(), 'link', true);


		echo "<div class='col $colClass'>" . get_the_post_thumbnail(get_the_id(), "relationships-thumb-$rowClass");

                if ($link != "") {
			echo "<a href='$link' target='_blank'>";
		}

		echo "<div class='client-overlay'><div class='overlay-text'>";
		echo "<h2 class='company'>" . get_the_title() . "</h3>";
		echo "<h3 class='location'>" . get_post_meta(get_the_ID(),'location', true) . "</h3>";
		echo get_post_meta(get_the_ID(),'relationships_hover', true);
		echo "</div></div>";
		
		if ($link != "") {
			echo "</a>";
		}

		echo "</div>";

		if ($c == $cols) {
			if ($cols==2) {$cols=3;} else {$cols=2;};
			$c=1;
                        echo "</div><!-- /row -->";
		} else { $c++; }

		endwhile;
	}
?>
    



	</div>
</div>
<?php endwhile; ?>
</div>
<?php get_footer(); ?>
