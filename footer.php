			<footer class="footer" role="contentinfo">

			<div id="inner-footer" class="wrap cf">
				<div id="footer-logo" class="m-all t-all d-1of3">
                                        <img src="/wp-content/uploads/2014/11/footer-logo.png"></img>
                                </div>
                                <div id="footer-nav" class="m-all t-all d-2of3 footer-links">
				        <div id="footer-menu-center-left" class="m-all t-all d-1of4"><?php wp_nav_menu(array('theme_location' => 'footer-left', 'menu-class' => 'nav')); ?></div>
				        <div id="footer-menu-center" class="m-all t-all d-1of4"><?php wp_nav_menu(array('theme_location' => 'footer-center-left')); ?></div>
				        <div id="footer-menu-center-right" class="m-ll t-all d-1of4"><?php wp_nav_menu(array('theme_location' => 'footer-center-right')); ?></div>
					<div id="footer-menu-right" class="m-all t-all-d-1of4">
						<?php wp_nav_menu(array('theme_location' => 'footer-right')); ?>
						<a class="footer-social-network fb" href="https://www.facebook.com/HarmonicEnergies" target="_blank">&nbsp;</a>
					</div>
                                </div>

			</div>

                        <div id="footer-bottom">&nbsp;</div>

			</footer>

		<!--/div> -->

		<div id="contact-popup" class="general">
                   <h1>Contact Us</h1>
		   <div class="main-content"><?php $content = get_post(71); echo apply_filters('the_content', $content->post_content); ?></div>
                </div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>
	</body>

</html> <!-- end of site. what a ride! -->
