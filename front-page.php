<?php get_header() ?>

<?php while(have_posts()) : the_post() ; ?>
<!--
We position the images fixed and therefore need to place them outside of #skrollr-body.
We will then use data-anchor-target to display the correct image matching the current section (.gap element).
-->
<div
class="parallax-image-wrapper parallax-image-wrapper-100"
data-anchor-target="#tagline + .gap"
data-bottom-top="transform:translate3d(0px, 200%, 0px)"
data-top-bottom="transform:translate3d(0px, 0%, 0px)"
>
	<div
		class="parallax-image parallax-image-100"
		style="background-image:url(/wp-content/uploads/2014/11/HARMN_Website-LandingImage.jpg)"
		data-anchor-target="#tagline + .gap"
		data-bottom-top="transform: translate3d(0px, -80%, 0px);"
		data-top-bottom="transform: translate3d(0px, 80%, 0px);"
	></div>
<!--the +/-80% translation can be adjusted to control the speed difference of the image-->
</div>
<div id="skrollr-body">
	<div class="m-none" id="tagline" data-80-top="opacity: 0;" data-center="opacity: 1">
		<div class="wrap cf">
			<div class="m-all t-all d-all">
				<?php echo get_post_meta(get_the_ID(), 'tagline', true); ?>
				<div class="scroll-down"><a href="#page-content"><img src="<?php echo get_template_directory_uri(); ?>/library/images/scroll-down.gif"</img></a></div>
			</div>
		</div>
	</div>
	<div class="gap gap-100" style="background-image:url(/wp-content/uploads/2014/11/HARMN_Website-LandingImage.jpg); top:0px;"></div>
</div>
<div id="normal-content">
	<div  id="page-content" class="section">
		<div class="wrap cf">
			<div class="m-all t-all d-all">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
	<div id="services-content" class="section">
        <h2 class="section-title">what we do</h2>
		<?php echo service_blocks(); ?>
	</div>
	<div id="clients-content" class="section">
		<div class="wrap cf">
			<div class="m-all t-all d-all">
			    <div id="client-block-container">
                                <div class="wrap cf">
                                <div class="client m-all t-all d-1of3"><a href="/our-relationships/"><img src="/wp-content/uploads/2014/11/powerhouse.png"></img></a></div>
                                <div class="client no-resize m-all t-all d-1of3"><a href="/our-relationships/"><img src="/wp-content/uploads/2014/11/clients_header.png"></img></a></div>
				<div class="client m-all t-all d-1of3"><a href="/our-relationships/"><img src="/wp-content/uploads/2014/11/seacoast.png"></img></a></div>
				</div>
                                <div class="wrap cf">
                                <div class="client m-all t-all d-1of5"><a href="/our-relationships/"><img src="/wp-content/uploads/2014/11/diningin.png"></img></a></div>
                                <div class="client m-all t-all d-1of5"><a href="/our-relationships/"><img src="/wp-content/uploads/2014/11/fsc.png"></img></a></div>
                                <div class="client m-all t-all d-1of5"><a href="/our-relationships/"><img src="/wp-content/uploads/2014/11/anchor.png"></img></a></div>
                                <div class="client m-all t-all d-1of5"><a href="/our-relationships/"><img src="/wp-content/uploads/2014/11/HARMN_Website-OurClients_19.png"></img></a></div>
				<div class="client m-all t-all d-1of5"><a href="/our-relationships/"><img src="/wp-content/uploads/2014/12/deco.png"></img></a></div>
                                </div>
                            </div>
			</div>
		</div>
	</div>
	<div id="project-content" class="section">
        <h2 class="section-title">featured project</h2>
		<div class="wrap cf">
			<div class="m-all t-all d-all">
                                
                                <?php $project=new WP_Query(array('post_type' => 'project', 'p' => 287, 'posts_per_page' => 1)); 

if( $project->have_posts() ) {
	while ($project->have_posts()) : $project->the_post(); ?>
            <div class="m-all t-all d-2of3 cf">
		<?php echo wp_get_attachment_image( get_post_meta(get_the_ID(), 'homepage_image', true), 'full'); ?>
	    </div>
	    <div id="project-text" class="m-all t-all d-1of3 cf">
		<h2>FEATURED WEBSITE:</h2>
                <h3>ANCHOR PEST MANAGEMENT</h3>
		<?php the_excerpt(); ?>
		<a class="button" href="<?php print get_post_meta(get_the_ID(), 'link', true); ?>">VISIT SITE</a>
            </div>
    <?php
  endwhile;
}
wp_reset_query();  // Restore global post data stomped </a></p>

                                ?>
                                
			</div>
		</div>
	</div>
</div>
<?php endwhile; ?>
<?php get_footer(); ?>
