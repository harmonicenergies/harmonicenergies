<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="cf">

						<div id="main" class="" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
                                                            <div id="header-wrap">
								<?php the_post_thumbnail('news-header'); ?>
								<header class="article-header" class="m-all t-all d-all">
                                                                        
									<h1 class="page-title"><?php the_title(); ?></h1>
                                                                        
                                                                        <br />
									<div class="byline vcard">
										<?php echo get_the_time('F d, Y'); ?>
									</div>

								</header>
                                                            </div>
								<div id="post-content-wrap" class="wrap cf">
								<div id="post-content" class="m-all t-all d-all"><?php the_content();?><a class="button readmore" href="<?php echo get_permalink( get_option('page_for_posts' ) ); ?>">More in the News</a></div>

                                                                </div>

							</article>

							<?php endwhile; endif; ?>

						</div>

				</div>

			</div>

<?php get_footer(); ?>
