<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="cf">

						<div id="main" class="" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
								<?php the_post_thumbnail('news-header'); ?>
								<header class="article-header" class="m-all t-all d-all">
                                                                        <div class="page-title">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                                                        </div>
                                                                        <br />
									<div class="byline vcard">
										<?php echo get_the_time('F d, Y'); ?>
									</div>

</header>

							</article>

							<?php endwhile; endif; ?>

						</div>

				</div>

			</div>

<?php get_footer(); ?>
