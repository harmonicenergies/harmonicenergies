<?php
    /*
    Template Name: Philosophy
     */
?>


<?php get_header() ?>
<?php while(have_posts()) : the_post() ; ?>
<?php
$thumb_id = get_post_thumbnail_id();
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'page-header', true);
$thumb_url = $thumb_url_array[0];
$color = get_post_meta(get_the_ID(), 'thought_point_color', true);
$blog = wp_get_recent_posts( array( 'numberposts' => 1) );
?>
<!--
We position the images fixed and therefore need to place them outside of #skrollr-body.
We will then use data-anchor-target to display the correct image matching the current section (.gap element).
-->
<div
class="parallax-image-wrapper parallax-image-wrapper-100"
data-anchor-target="#tagline + .gap"
data-bottom-top="transform:translate3d(0px, 200%, 0px)"
data-top-bottom="transform:translate3d(0px, 0%, 0px)"
>
	<div
		class="parallax-image parallax-image-100"
			style="background-image:url(<?php echo $thumb_url; ?>)"
		data-anchor-target="#tagline + .gap"
		data-bottom-top="transform: translate3d(0px, -80%, 0px);"
		data-top-bottom="transform: translate3d(0px, 80%, 0px);"
	></div>
<!--the +/-80% translation can be adjusted to control the speed difference of the image-->
</div>
<div id="skrollr-body" class="philosophy">
	<div class="m-none" id="tagline" data-10-top="opacity: 0;" data-center="opacity: 1">
		<div class="wrap cf">
			<div class="m-all t-all d-all">
				<div class="page-header"><?php echo get_post_meta(get_the_ID(), 'tagline', true); ?></div>
				<div class="scroll-down"><a href="#page-content"><img src="<?php echo get_template_directory_uri(); ?>/library/images/scroll-down.gif"/></a></div>
			</div>
		</div>
	</div>
	<div class="gap gap-100" style="background-image:url(<?php echo $thumb_url; ?>)"></div>
	<div id="page-content" class="section">
		<div class="wrap cf"><h2 class="section-title"><?php echo the_title(); ?></h2></div>
                <div class="wrap cf">
			<div class="m-all t-all d-1of2 main-content">
                                <h4>Orchestral Harmonies</h4>
				<?php the_content(); ?>
			</div>
			<div class="m-all t-all d-1of2">
				<h4>The Instruments</h4>
				<div id="the-instruments">
				    <a href="/services/website-creation/" id="website-creation">Website Creation</a><br />
				    <a href="/services/social-media/" id="social-media">Social Media</a><br />
				    <a href="/services/seoppc/" id="seo-ppc">SEO/PPC</a><br />
				    <a href="/services/branding-and-identity/" id="branding-identity">Branding & Identity</a><br />
				    <a href="/services/roi-tools/" id="roi-tools">ROI Tools</a><br />
                                    <a href="/services/education/" id="education">Education</a><br />
                                </div>
                        </div>
                </div>
	</div>
	<div id="page-buckets" class="wrap cf">
	<div class="bucket-left m-all t-all d-1of3"><hr></hr><h5>Seen and Heard</h5>"Harmonic Energies’ ability to quickly understand our company’s sales goals, target audiences, and services truly set their company apart from their competitors. Their seamless process of website design, implementation, and marketing campaigns has given our company an end product that has shown direct results in our sales and client’s education." <strong><i>Powerhouse Recycling</i></strong></div>
	<div class="bucket-center m-all t-all d-1of3">
            <hr></hr>
	    <h5>LATEST NEWS</h5> 
	    <?php foreach ($blog as $b) { echo get_the_post_thumbnail($b["ID"], 'service-thumb') . '<div id="title-wrap"><a class="page-title" href="'. get_the_permalink($b["ID"]) . '">' . get_the_title($b["ID"]) . '</a></div>'; }?>
        </div>
	<div class="bucket-right m-all t-all d-1of3"><hr></hr><h5>We'd love to meet you</h5>Our favorite part of what we do is the relationships we have formed with our clients over the years. We would love to meet with you and learn about what you do and why you do it.  Whether you’re starting a business for the first time or you're a nationally recognized brand, we are here to help bring you closer to your customers than ever before. <a href="/contact-us/" class="read-more contact-button">Let's Talk</a></div>
        </div>
        </div>
</div>
<?php endwhile; ?>
<?php get_footer(); ?>
